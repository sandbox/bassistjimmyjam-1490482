<?php

/**
 * @file
 * Provides metadata for the project entity type.
 */

/**
 * Implements hook_entity_property_info().
 */
function project_management_project_entity_property_info() {
  $info = array();

  // Add meta-data about the basic project_management_project properties.
  $properties = &$info['project_management_project']['properties'];

  $properties['project_id'] = array(
    'type' => 'integer',
    'label' => t('Project ID'),
    'description' => t('The internal numeric ID of the project.'),
    'schema field' => 'project_id',
  );
  $properties['project_number'] = array(
    'type' => 'text',
    'label' => t('Project number'),
    'description' => t('The project number displayed to the user.'),
    'setter callback' => 'entity_property_verbatim_set',
    'required' => TRUE,
    'schema field' => 'project_number',
  );
  $properties['title'] = array(
    'type' => 'text',
    'label' => t('Title'),
    'description' => t('The project title.'),
    'setter callback' => 'entity_property_verbatim_set',
    'required' => TRUE,
    'schema field' => 'title',
  );
  $properties['type'] = array(
    'type' => 'text',
    'label' => t('Type'),
    'description' => t('The human readable name of the project type.'),
    'setter callback' => 'entity_property_verbatim_set',
    'options list' => 'project_management_project_type_options_list',
    'required' => TRUE,
    'schema field' => 'type',
  );
  $properties['uid'] = array(
    'type' => 'integer',
    'label' => t('Owner'),
    'description' => t('The unique ID of the project owner.'),
    'setter callback' => 'entity_property_verbatim_set',
    'clear' => array('owner'),
    'schema field' => 'uid',
  );
  $properties['status'] = array(
    'type' => 'text',
    'label' => t('Status'),
    'description' => t('The current status of the project.'),
    'setter callback' => 'entity_property_verbatim_set',
    'options list' => 'project_management_project_status_options_list',
    'required' => TRUE,
    'schema field' => 'status',
  );
  $properties['created'] = array(
    'type' => 'date',
    'label' => t('Date created'),
    'description' => t('The date the project was created.'),
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'created',
  );
  $properties['changed'] = array(
    'type' => 'date',
    'label' => t('Date changed'),
    'description' => t('The date the project was most recently updated.'),
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'changed',
  );
  $properties['owner'] = array(
    'type' => 'user',
    'label' => t('Owner'),
    'description' => t('The owner of the project.'),
    'getter callback' => 'project_management_project_get_properties',
    'setter callback' => 'project_management_project_set_properties',
    'clear' => array('uid'),
    'required' => TRUE,
  );

  return $info;
}
